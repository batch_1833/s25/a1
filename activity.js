// Use the count operator to count the total number of fruits on sale.

db.fruits.aggregate( [
   {$match: {"onSale": true}},
   { $group: { _id: null, fruitOnSale: { $sum: 1 } } },
   { $project: { _id: 0 } }
] )

// Use the count operator to count the total number of fruits with stock more than 20.\

db.fruits.aggregate( [
    {$match: {"stocks": {$gte: 20 } } },
    {$count: "enoughStocks"}
 ] )

// Use the average operator to get the average price of fruits onSale per supplier.



db.fruits.aggregate([
	{$group: {"onSale": true,
           
           avgAmount: { $avg: { $multiply: [ "$price", "$stocks" ] } },
           avgStocks: { $avg: "$stocks" }
         }
     }
   ]
)